# -*- elpy-test-runner: elpy-test-pytest-runner -*-
import marko.library as lib
import marko.collection as coll
import pytest
from pathlib import Path


class TestLibrary:

    def test_addField(self, tLib):
        """
        Field names should be added to all items in library
        """
        newfield = 'louis XV'
        tLib.addField(newfield)
        for f in tLib.files:
            assert newfield in f.fields.keys()
            for m in f.marks:
                assert newfield in m.fields.keys()

    def test_delField(self, tLib):
        """
        Field names should be deleted from all items in library
        """
        oldfield = list(tLib.fnames)[0]
        tLib.delField(oldfield)
        for f in tLib.files:
            assert oldfield not in f.fields.keys()
            for m in f.marks:
                assert oldfield not in m.fields.keys()

    def testScanDirRecursive(self, tLibWMF):
        """
        Test library should see 26 files (all except Iznogud.txt)
        """
        tLib = tLibWMF[0]
        assert len(tLib.files) == 26

    def testScanDirIgnoreSuffixCase(self, tmpdir):
        """
        Files should be found by suffix whatever the suffix case
        """
        ppath = Path(str(tmpdir))
        # create 3 files with various cases in suffix
        mixedcase = Path(ppath).joinpath('MixedCase.mP3')
        upper = Path(ppath).joinpath('UppEr.OGG')
        lower = Path(ppath).joinpath('LoWer.ogg')
        mixedcase.touch()
        upper.touch()
        lower.touch()
        l = lib.Library()
        l.atypes = l.mtypes = {'.mp3', '.ogg'}
        result = l.scanDirs([ppath])
        resultpaths = {str(f.ppath) for f in result}
        assert {str(upper), str(lower), str(mixedcase)} == resultpaths

    def testPathNotDir(self):
        """
        createLibrary should raise exception if ppath doesn’t exist
        """
        l = lib.Library()
        p = Path('ElephantVertSurLeMur')
        with pytest.raises(coll.InvalidPathError):
            l.createLibrary([p])

    def test_LibraryRW(self, tLibWMF, orderCollDictsAndSets):
        """
        Write Library to disk then read it back should yeld equivalent Library
        """
        tLib = tLibWMF[0]  # keep only collection for this one
        tLib.write()
        newLib = lib.Library().read(tLib.storage)

        # convert unordered items to ordered ones
        orderCollDictsAndSets(tLib)
        orderCollDictsAndSets(newLib)

        assert repr(tLib) == repr(newLib)
