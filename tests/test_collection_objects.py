# -*- elpy-test-runner: elpy-test-pytest-runner -*-
from marko.collection_objects import CollObj, File
from pathlib import Path
import pytest


class TestCollObj:
    def test_initJustFnames(self):
        """
        CollObj should be properly initialized when given a list of field names
        """
        fnames = {'foo', 'bar', 'baz'}
        obj = CollObj(fnames)
        for name in fnames:
            assert obj.fields[name] is None

    def test_initFullFields(self):
        """
        CollObj should be properly initialized when given a dict
        """
        fields = {'foo': 'bar', 'bingo': 'banga'}
        obj = CollObj(fields)
        for key, value in fields.items():
            assert obj.fields[key] == value

    def test_writeNonExistingFname(self):
        """
        Set value should work only for existing fields
        """
        fnames = {'foo', 'bar', 'baz'}
        obj = CollObj(fnames)

        obj.setFields(foo='monty')
        print(obj.fields)
        assert obj.fields['foo'] == 'monty'

        spam = 'spam'
        obj.setFields(spam='python')
        assert spam not in obj.fields


class TestFile:
    @pytest.fixture
    def tFile(self):
        """
        Fixture to make test File
        """
        fnames = {'foo', 'bar', 'baz'}
        values = {'œuf', 'Şûkāĳ', 'αβι δγ'}
        tFile = File(fnames)
        for f, v in zip(fnames, values):
            tFile.setFields(f=v)
        tFile.type = 'audio'
        tFile.ppath = Path('/my/test/file')
        tFile.name = tFile.ppath.stem
        # Add 5 markers
        for x in range(5):
            tFile.addMark(x)
            # set field on last mark
            tFile.marks[-1].setFields(foo='eggs', bar=x)
        return tFile

    def test_addMark(self, tFile):
        """
        Add 5 marks to test File: it should have 10 marks
        """
        for x in range(5, 10):
            tFile.addMark(x)
        assert len(tFile.marks) == 10

    def test_delMark(self, tFile):
        """
        Delete 1 mark from test File should erase corresponding timing
        """
        mark = tFile.marks[1]
        start = mark.start
        tFile.delMark(mark)
        remaining_starts = [m.start for m in tFile.marks]
        assert start not in remaining_starts

    def test_addMarkStartExists(self, tFile):
        """
        Add mark with existing start position should increment start by 1
        """
        tFile.addMark(1)
        assert len(tFile.marks) == 6
        assert tFile.marks[-1].start == 5

    def test_addMarkStopExists(self, tFile):
        """
        Add mark with existing stop position should decrement stop by 1
        """
        tFile.addMark(6, 10)
        tFile.addMark(7, 10)
        assert len(tFile.marks) == 7
        assert tFile.marks[-1].stop == 9

    def test_addMarkStartIsStop(self, tFile):
        """
        Add mark with same start as existing stop should increment start by 1
        """
        tFile.addMark(6, 10)
        tFile.addMark(10)
        assert len(tFile.marks) == 7
        assert tFile.marks[-1].start == 11

    def test_FileAndMarkFieldsIndependent(self, tFile):
        """
        Mark fields and File fields should not point to same dict
        """
        fname = list(tFile.fields.keys())[0]
        newmark = tFile.addMark(20)
        tFile.fields[fname] = 'foo'
        newmark.fields[fname] = 'bar'
        assert tFile.fields[fname] == 'foo'
        assert newmark.fields[fname] == 'bar'
        assert newmark in tFile.marks
