import marko.library as lib
import pytest
from pathlib import Path

TMEDIA = 'tests/tmedia/'


@pytest.fixture
def tLib(tmpdir):
    """Test Library. No Marks, no fields set."""
    global TMEDIA
    path = Path(TMEDIA).absolute()
    tdir = Path(str(tmpdir))
    tLib = lib.Library()
    tLib.vtypes = {'.mp4', '.avi', '.flv', '.mov'}
    tLib.atypes = {'.mp3', '.wav', '.aiff'}
    tLib.fnames = {'name', 'director', 'composer', 'dj'}
    tLib.mtypes = tLib.vtypes.union(tLib.atypes)
    tLib.createLibrary({path})
    tLib.storage = tdir.joinpath("tlib.yaml")
    return tLib


@pytest.fixture
def tLibWMF(tLib, tmpdir):
    """
    tLib With Marks and a few fields set. Return tuple:
    [0]: Library
    [1]: dict of files/marks for which fieldname was set to value :
         { (fieldname, value): {file, file, mark, mark, etc.} }
    [2]: all field names (set)
    [3]: all field values (set)
    """
    tLibWMF = tLib
    fnames = tLibWMF.fnames
    for file in tLib.files:
        for x in range(5):
            file.addMark(0)
    some_values = {'cat', 'elephant', 'monkey'}

    # For later checks: initialize field_sets as a dict of (key, value): set
    # Assign randomly our values to the fields of each library items
    field_sets = dict()
    import random
    for file in tLibWMF.files:
        # Set two fields randomly
        for rname in random.sample(fnames, 2):
            rvalue = random.sample(some_values, 1)[0]
            file.fields[rname] = rvalue  # bypassing setFields to use rname var
            if (rname, rvalue) not in field_sets.keys():
                field_sets[(rname, rvalue)] = set()
            field_sets[(rname, rvalue)].add(file)
        for mark in file.marks:
            for rname in random.sample(fnames, 2):
                rvalue = random.sample(some_values, 1)[0]
                mark.fields[rname] = rvalue
                if (rname, rvalue) not in field_sets.keys():
                    field_sets[(rname, rvalue)] = set()
                field_sets[(rname, rvalue)].add(mark)

    storage = Path(str(tmpdir)).joinpath('tLibWMF_from_fixture.yaml')
    tLibWMF.write(storage)
    return (tLibWMF, field_sets, fnames, some_values)


@pytest.fixture
def orderCollDictsAndSets():
    """
    Returns helper function to onvert unordered dicts/sets to
    their ordered counterparts. Useful to assert collection
    equivalence through repr(coll_A) == repr(coll_B)
    """

    def orderIt(unordered_coll):
        from collections import OrderedDict

        def order(value):
            if isinstance(value, dict):
                value = OrderedDict(sorted(value.items()))
            elif isinstance(value, set):
                value = sorted(value)
            else:
                pass
            return value

        # Check attributes of collection, files and marks
        for attr, value in vars(unordered_coll).items():
            setattr(unordered_coll, attr, order(value))
            for file in unordered_coll.files:
                for attr, value in vars(file).items():
                    setattr(file, attr, order(value))
                    for mark in file.marks:
                        for attr, value in vars(mark).items():
                            setattr(mark, attr, order(value))
    return orderIt
