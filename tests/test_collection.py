# -*- elpy-test-runner: elpy-test-pytest-runner -*-
import marko.collection as coll
import pytest
from pathlib import Path


@pytest.fixture
def tColl(tLibWMF, tmpdir):
    """
    Fixture to make test Collection.
    Actually uses preexisting fixture to make test Library
    """
    tColl = coll.Collection()
    tColl.files = tLibWMF[0].files
    field_sets = tLibWMF[1]
    field_names = tLibWMF[2]
    field_values = tLibWMF[3]
    tColl.storage = Path(str(tmpdir)).joinpath('tColl_from_fixture.yaml')
    return (tColl, field_sets, field_names, field_values)


class TestCollection:

    def test_getFieldNames(self, tColl):
        """
        getFieldNames() should return all Field names in Collection
        """
        expected_fnames = tColl[2]
        fnames = tColl[0].getFieldNames()
        assert fnames == expected_fnames

    def test_fieldMatching(self, tLibWMF):
        """
        fieldMatching() should return all files/marks where field set to value
        """
        tColl = tLibWMF[0]
        expected_results = tLibWMF[1]
        tColl.files = tColl.files  # give our collection the same fileset

        # make tuples of all field-value combinations in Library
        criterions = set()
        for name in tColl.fnames:
            for value in tColl.getValueSet(name):
                criterions.add((name, value))

        # make collection for each criterion
        for crit in criterions:
            matched = tColl.fieldMatching(crit)
            # test
            assert matched == expected_results[crit]

    def test_ppathMatching(self, tLibWMF, tColl):
        """
        ppathMatching() should return all files under given dir
        """
        myColl = tColl[0]

        # used only to gather expected results by actually walking the path
        mtypes = tLibWMF[0].mtypes
        ppaths = tLibWMF[0].ppaths

        # Walk root path for expected results
        expected = dict()
        for ppath in ppaths:
            expected[str(ppath)] = {str(path) for path in ppath.glob('*')
                                    if path.is_file() and
                                    path.suffix.lower() in mtypes}
            for dir in {d for d in ppath.rglob('*') if d.is_dir()}:
                expected[str(dir)] = {str(path) for path in dir.glob('*')
                                      if path.is_file() and
                                      path.suffix.lower() in mtypes}

        # Walk library and assert
        results = dict()
        for file in myColl.files:
            filedir_ppath = file.ppath.parent
            results[filedir_ppath] = set()
        for dir_ppath in results.keys():
            matched_files = myColl.ppathMatching(dir_ppath)
            matched_paths = {str(match.ppath) for match in matched_files}
            assert matched_paths == expected[str(dir_ppath)]

    # def test_ppathTree(self, tLib, tColl):
    #     tColl.files = tLib.files
    #     result = tColl.files.ppathTreeMaker()
    #     types = tLib.types
    #     base = tLib.ppaths

    def test_getValueSet(self, tColl):
        """
        getValueSet() should return all values in Library for given field
        """
        myColl = tColl[0]
        myColl.fnames = myColl.getFieldNames()
        expected = set(tColl[1].keys())
        # Generate all possible (key, value) tuples from our collection
        output = set()
        for name in myColl.fnames:
            for value in myColl.getValueSet(name):
                output.add((name, value))
        assert output == expected

    def test_DirTree(self, tLibWMF):
        # Don’t use tColl fixture here (ensure same ppath root)
        tColl = coll.Collection()
        tColl.files = tLibWMF[0].files
        libroots = tLibWMF[0].ppaths

        def dirWalk(current_dir):
            "recursively gather dirs in lists. dirs with subdirs enter the list as dicts."
            current_level = list()
            for child in current_dir.iterdir():
                if child.is_dir():
                    walk = dirWalk(child)
                    if walk:
                        newdict = {str(child): walk}
                        current_level.append(newdict)
                    else:
                        current_level.append(str(child))
            return current_level

        ppath_dict = dict()
        for rootdir in libroots:
            ppath_dict[str(rootdir)] = dirWalk(rootdir)

        # ppath_dict: {dir: [subdir w. no sub,
        #                    {subdir w. sub: [subdir no sub,
        #                                     subdir no sub]},
        #                    subdir w. no sub,]
        #              dir: etc.}

        # from pprint import pprint
        # pprint(ppath_dict)
        # assert 0

    def test_CollectionRW(self, tColl, orderCollDictsAndSets):
        """
        Write collection to disk then read it back should yeld equivalent Collections
        """
        tColl = tColl[0]  # keep only collection for this one
        tColl.write()
        newColl = coll.Collection().read(tColl.storage)

        # convert unordered items to ordered ones
        orderCollDictsAndSets(tColl)
        orderCollDictsAndSets(newColl)

        assert repr(tColl) == repr(newColl)

    # def test_fieldValueCollection(self, tLibWMF, tColl):
    #     tLib = tLibWMF[0]
    #     configured_fields = tLibWMF[1]
    #     tColl.files = tLib.files
    #     for set_tuple in configured_fields.keys():
    #         expected = [f for f in configured_fields[set_tuple]
    # if isinstance(f, coll.File)]  # set to list
    #         expected.sort(key=attrgetter('name'))
    #         output = tColl.fieldValueCollection(tLib.files, set_tuple)
    #         assert output == expected

#
# structure = {
#     'dir': {
#         'filepath': {
#             fields: [
#                 list
#             ]
#             markers:
#             {
#                 0: [
#                     fields
#                 ]
#                 3: [
#                     fields
#                 ]
#             }
#         }
#         'subdirB': {
#             'filepathX': {
#                 fields: [
#                     list
#                 ]
#                 markers:
#                 {
#                     0: [
#                         fields
#                     ]
#                     3: [
#                         fields
#                     ]
#                 }
#             }

#             'filepathY': {
#                 fields: [
#                     list
#                 ]
#                 markers:
#                 {
#                     0: [
#                         fields
#                     ]
#                     3: [
#                         fields
#                     ]
#                 }
#             }
#         }
#         'subdirC': {
#             'filepathZ': {
#                 fields: [
#                     list
#                 ]
#                 markers:
#                 {
#                     0: [
#                         fields
#                     ]
#                     3: [
#                         fields
#                     ]
#                 }
#             }

#             'filepathK': {
#                 fields: [
#                     list
#                 ]
#                 markers:
#                 {
#                     0: [
#                         fields
#                     ]
#                     3: [
#                         fields
#                     ]
#                 }
#             }
#         }
#     }
# }
