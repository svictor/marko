# -*- elpy-test-runner: elpy-test-pytest-runner -*-
from marko.collection_objects import File
from operator import attrgetter
import yaml


class CollectionError(Exception):
    pass


class InvalidPathError(CollectionError):
    pass


class InvalidInputError(CollectionError):
    pass


class InvalidMarkError(CollectionError):
    pass


class Collection:

    """
    A Collection holds a list of Files and various methods to access them
    """

    def __init__(self):
        self.files = list()
        self.storage = None  # pathlib Path to write the Library to disk
        self.sortorder = 'default'

    def __repr__(self):
        return "{0}(storage={1!r}, sortorder={2!r}, files={3!r})".format(
            self.__class__.__name__,
            self.storage,
            self.sortorder,
            self.files
        )

    def fieldMatching(self, crit):
        """
        Returns a set of Files/Marks whose fields match criterion.
        Criterion is tuple (key, value)
        """
        key = crit[0]
        value = crit[1]
        matched = set()

        for file in self.files:
            if file.fields[key] == value:
                matched.add(file)
            for mark in file.marks:
                if mark.fields[key] == value:
                    matched.add(mark)
        return matched

    def fieldMatch(self, element, crit):
        """
        Evaluate whether element matches criterion
        """
        key = crit[0]
        value = crit[1]
        if element.fields[key] == value:
            return True
        else:
            return False

    def ppathMatching(self, ppath):
        """
        Returns set of files matching ppath directory
        """
        if not ppath.is_dir():  # TODO: handle exception properly (if needed)
            raise InvalidPathError("%s is not a directory" % str(ppath))
        fileset = set()
        for file in self.files:
            if file.ppath.parent.samefile(ppath):
                fileset.add(file)
        return fileset

    def sortFiles(self, sortorder=None):
        if sortorder == 'default':
            self.files.sort(key=attrgetter('name'))
        return None

    def getFieldNames(self):
        "Return set of all field names in Collection"
        fnames = set()
        for file in self.files:
            for name in file.fields.keys():
                fnames.add(name)
            for mark in file.marks:
                for name in mark.fields.keys():
                    fnames.add(name)
        return fnames

    def getValueSet(self, fname):
        "Return set of all values of field except None"
        value_set = set()
        for file in self.files:
            for mark in file.marks:
                value_set.add(mark.fields[fname])
            value_set.add(file.fields[fname])
        value_set.discard(None)
        return value_set

    def stats(self):
        """
        Return number of files and markers in the Collection
        """
        nfiles = len(self.files)
        nmarks = 0
        for file in self.files:
            nmarks += len(file.marks)
        print("Library has %i files and %i marks" % (nfiles, nmarks))
        return (nfiles, nmarks)

    def makeFile(self, fields=None):
        """
        Add File to Collection
        """
        file = File(fields)
        return file

    def write(self, storage=None):
        """
        Write Collection to yaml file.
        All objects in Collection should have details __repr__ methods
        """
        if not storage:
            storage = self.storage
        try:
            wfile = storage.open('w')
        except:
            raise InvalidPathError("Can’t storage path %s" % storage)
        yaml.dump(self, wfile, indent=4)
        return None

    def read(self, storage):
        """
        Read Collection from yaml file.
        Objects are recreated automagically provided their
        __init__() args match their __repr__() fields
        """

        with storage.open('r') as rfile:
            data = yaml.load(rfile)
        return data


class Shadows(Collection):

    """Tuple of elements shadowed/unshadowed according to criterion"""

    def __init__(self, fileset, criterion=None):
        super().__init__(fileset, criterion)
        self.shadows = tuple(set(), set())  # (shadowed, unshadowed)
        for file in fileset:
            self.shadow(file)

    def shadow(self, file):
        """Add file shadowing it or not according to criterion"""
        if self.fieldMatch(file):
            self.shadows[0].add(file)
        else:
            self.shadows[1].add(file)
        for mark in file.marks:
            if self.fieldMatch(mark):
                self.shadows[0].add(mark)
            else:
                self.shadows[1].add(mark)
