# -*- elpy-test-runner: elpy-test-pytest-runner -*-
"""
A File represents a file on disk.
A Mark represents a start and (optional) end position in a File.
Files and Marks have descriptive fields. Field names are common to all Library items.
"""
# from marko.collection import InvalidInputError, InvalidMarkError
import hashlib
from operator import attrgetter  # needed for sort


class CollObj:

    """
    Class inherited by Files and Marks
    """

    def __init__(self, fields):
        if isinstance(fields, set):
            # Init fields just by name
            fnames = fields
            self.fields = dict()
            for name in fnames:
                self.fields[name] = None
        elif isinstance(fields, dict):
            self.fields = fields
        else:
            raise InvalidInputError("CollObj needs a set or a dict")

    def __repr__(self):
        return "{0}(Fields={1!r})".format(
            self.__class__.__name__,
            self.fields
        )

    def setFields(self, **kwargs):
        for name, value in kwargs.items():
            if name in self.fields:
                self.fields[name] = value


class Mark(CollObj):

    """
    A Mark is attached to a File. It has a start and optional stop value.
    Timing is set from File to allow checking for duplicate timings.
    """

    def __init__(self, fields):
        super().__init__(fields)
        self.start = None
        self.stop = None

    def __repr__(self):
        return "{0}(Fields={1!r}, Start={2!r}, Stop={3!r})".format(
            self.__class__.__name__,
            self.fields,
            self.start,
            self.stop,
        )


class File(CollObj):

    """
    A File represents a file on disk.
    """

    def __init__(self,
                 fields,
                 marks=None,
                 ppath=None,
                 md5=None,
                 name=None,
                 mtype=None):

        super().__init__(fields)
        if marks:
            self.marks = marks
        else:
            self.marks = list()
        if ppath:
            self.ppath = ppath
        else:
            self.ppath = None
        if md5:
            self.md5 = md5
        else:
            self.md5 = None
        if name:
            self.name = name
        else:
            self.name = str()
        if mtype:
            self.mtype = mtype
        else:
            self.mtype = 'unknown'

    def __repr__(self):
        return "{0}(path={1!r}, md5={2!r}, name={3!r}, mtype={4!r}, fields={5!r}, marks={6!r})".format(
            self.__class__.__name__,
            self.ppath,
            self.md5,
            self.name,
            self.mtype,
            self.fields,
            self.marks,
        )

    def getMd5(self, ppath=None):
        """
        Get md5sum at pathlib path. Defaults to file’s path.
        """
        if not ppath:
            ppath = self.ppath
        md5 = hashlib.md5()
        BUF_SIZE = 65536  # read file in 64kb chunks
        with ppath.open('rb') as f:
            while True:
                data = f.read(BUF_SIZE)
                if not data:
                    break
                md5.update(data)
        return md5.hexdigest()

    def addMark(self, start, stop=None):
        """
        Adds a mark
        """
        fields = self.fields.copy()  # initialize values to None maybe?
        mark = Mark(fields)
        self.setMarkTime(mark, start, stop)
        self.marks.append(mark)
        return mark

    def delMark(self, mark):
        """
        Deletes a mark
        """
        self.marks.remove(mark)
        return None

    def setMarkTime(self, mark, start, stop=None):
        """
        Sets start and stop timings on a mark
        If other marks start at same time, nudge the start later.
        If other markes end at same time, nudge start earlier.
        """
        start_positions = [m.start for m in self.marks]
        stop_positions = [m.stop for m in self.marks]
        positions = start_positions + stop_positions
        while start in positions:
            start += 1
        positions.append(start)
        while stop and stop in positions:
            stop -= 1

        if stop and stop < start:
            raise InvalidMarkError("Stop before start")

        mark.start = start
        mark.stop = stop
        self.marks.sort(key=attrgetter('start'))
        return None
