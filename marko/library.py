# -*- elpy-test-runner: elpy-test-pytest-runner -*-
"""
A Library is the Collection of all known Files
"""

from pathlib import Path
from marko.collection import Collection, InvalidPathError


class Library(Collection):

    '''
    The Library is the Collection of all Files.
    '''

    def __init__(self, ppaths=None, fnames=None, atypes=None, vtypes=None):
        super().__init__()
        if ppaths:  # pathlib paths
            self.ppaths = {p.absolute() for p in ppaths}
        else:
            self.ppaths = set()
        if fnames:  # field names
            self.fnames = fnames
        else:
            self.fnames = {'name', 'context', 'personal'}
        if atypes:  # audio types
            self.atypes = atypes
        else:
            self.atypes = {'.mp3', '.wav', '.aiff'}
        if vtypes:  # video types
            self.vtypes = vtypes
        else:
            self.vtypes = {'.mp4', '.avi', '.flv', '.mov'}

        self.mtypes = self.atypes.union(self.vtypes)

    def __repr__(self):
        return "{0}(ppaths={1!r}, atypes={2!r}, vtypes={3!r}, fnames={4!r}, files={5!r})".format(
            self.__class__.__name__,
            self.ppaths,
            self.atypes,
            self.vtypes,
            self.fnames,
            self.files
        )

    def createLibrary(self, ppaths=None):
        """Populate the Library by scanning ppaths"""
        if not ppaths:
            ppaths = self.ppaths
        else:
            self.ppaths = ppaths  # Todo: check how to handle ppath change
        self.files = self.scanDirs(ppaths)
        return None

    def refreshLibrary(self, ppaths):
        """Todo: rescan ppaths to find new/deleted/moved files"""
        pass

    def scanDirs(self, ppaths):
        """Scan a list of ppaths. Return the list of matched files"""
        fnames = self.fnames
        # Store ppaths globally
        self.ppaths = ppaths

        filescan = list()
        # Scan ppaths
        for ppath in ppaths:
            if not ppath.exists():
                raise InvalidPathError("Path %s does not exist" % ppath)
            if not ppath.is_dir():
                raise InvalidPathError("Path %s is not a directory" % ppath)

            matches = {p for p in ppath.rglob('*')
                       if p.is_file()
                       and p.suffix.lower() in self.mtypes}
            for match in matches:
                file = self.makeFile(fnames)
                file.ppath = match.absolute()
                file.name = match.stem
                file.md5 = file.getMd5(match)
                if match.suffix in self.atypes:
                    file.mtype = 'audio'
                elif match.suffix in self.vtypes:
                    file.mtype = 'video'
                filescan.append(file)

        return filescan

    def addField(self, fname):
        """Add a field recursively to Files & Marks"""
        if fname in self.fnames:
            return
        else:
            for f in self.files:
                f.fields[fname] = None
                for m in f.marks:
                    m.fields[fname] = None
        self.fnames.add(fname)
        return None

    def delField(self, fname):
        """Delete a field recursively from Files & Marks"""
        if fname not in self.fnames:
            return
        else:
            for f in self.files:
                f.fields.pop(fname)
                for m in f.marks:
                    m.fields.pop(fname)
        self.fnames.remove(fname)
        return None

    def renameField(self, fname):
        pass


if __name__ == '__main__':
    p = Path('tests/tmedia/Afolder/Aasubfolder')
    l = Library([p])
    l.createLibrary()
    l.storage = Path('test.yaml')
    l.write()
    r = Library()
    s = r.read(l.storage)
